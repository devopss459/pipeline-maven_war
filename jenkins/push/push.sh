#!/bin/bash

echo "********************"
echo "** Pushing image ***"
echo "********************"

IMAGE="maven-project"

echo "** Logging in ***"
docker login -u nikarora -p $PASS
echo "*** Tagging image ***"
docker tag $IMAGE:$BUILD_TAG nikarora/$IMAGE:$BUILD_TAG
echo "*** Pushing image ***"
docker push nikarora/$IMAGE:$BUILD_TAG

