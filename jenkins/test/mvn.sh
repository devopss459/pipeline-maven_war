#!/bin/bash

echo "***************************"
echo "** Testing the code ***********"
echo "***************************"
WORKSPACE=/home/ec2-user/devops/jenkins_home/workspace/pipeline-maven1

docker run --rm  -v  $WORKSPACE/java_app:/app -v /root/.m2/:/root/.m2/ -w /app maven:3-alpine "$@"

